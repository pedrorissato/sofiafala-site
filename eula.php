<html>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
<title>Projeto Sofia - EULA</title>
<meta charset="utf-8">

	<?php include_once("resources/template/header.php"); ?>
</head>

<body>
<h1 style="text-align: center"><b>Contrato de Licença de Uso de Software</b></h1>

<p>Esta Licença de Uso de Software deve ser lida cuidadosamente antes da instalação ou utilização do aplicativo SofiaFala que a acompanha. A utilização do software, ainda que parcialmente ou a título de “teste”, indica que o usuário compreende e concorda com as condições desta Licença. Caso discorde das condições da Licença, o software não deverá ser instalado e/ou utilizado.</p>

<br />

<p>1. Esta Licença constitui um acordo legal entre a pessoa física (denominado aqui usuário) e o grupo de pesquisa "SofiaFala", registrado sob o processo CNPq nº 442533/2016, vinculado ao Departamento de Computação e Matemática da Faculdade de Filosofia, Ciências e Letras de Ribeirão Preto da Universidade de São Paulo (DCM-FFCLRP-USP),  para uso restrito a fins de ensino, complementar ou não, a jovens e adultos com apraxia da fala.</p>
<p>2. Esta Licença não poderá ser concedida a pessoa jurídica, ainda que sem fins lucrativos. Caso uma pessoa jurídica tenha interesse no uso do aplicativo SofiaFala, deverá entrar em contato com o grupo SofiaFala pelo email sofiafala.contato@gmail.com O o aplicativo SofiaFala foi desenvolvido para ajudar no treinamento da fala de jovens e adultos com apraxia de fala. Sua utilização pelo usuário será concedida de forma gratuita, por meio do endereço eletrônico http://dcm.ffclrp.usp.br/sofiafala/, desde que respeitadas as condições desta Licença.
<p>3. A Licença concedida pelo grupo SofiaFala não constitui uma cessão ou outorga, ainda que parcial, de direitos de propriedade intelectual do aplicativo SofiaFala, ou seja, não se transfere direitos sobre o software para o usuário a partir da Licença. O grupo SofiaFala é, e permanecerá, sendo a única proprietária dos direitos de propriedade intelectual sobre o software, independentemente do mesmo estar registrado ou não em qualquer país do mundo.</p>
<p>4. Em nenhuma hipótese o usuário terá acesso ao código fonte do aplicativo SofiaFala.</p>
<p>5. A Licença permitirá ao usuário baixar, instalar e utilizar o SofiaFala em seu celular. O usuário se compromete a não realizar, causar, permitir ou autorizar a modificação, criação de trabalhos derivados ou aprimoramentos, tradução, engenharia reversa, descompilação, desmontagem, decodificação, emulação, hacking, descoberta ou tentativa de descoberta do código-fonte ou protocolos do software ou qualquer parte ou recursos deste, exceto na medida permitida por lei.</p>
<p>6. É proibido o uso comercial, bem como a transferência do software para outra pessoa física ou jurídica. O usuário responderá civil e criminalmente pelo descumprimento das condições desta Licença, obrigando-se a indenizar o grupo SofiaFala, pelas perdas e dados em caso de uso comercial e/ou de transferência do software, em benefício próprio ou de terceiro.</p>
<p>7. Qualquer divulgação ou citação do SofiaFala deverá mencionar o grupo, a USP e o CNPq. Ainda, poderão ser mencionados os autores do software, idealizadores do projeto.</p>
<p>8. O acesso ao software ocorrerá após a aceitação das condições desta Licença e ao cadastro no site. Após esses procedimentos, o usuário poderá efetuar o download do programa em seu computador.</p>
<p>9. A Licença poderá ser modificada pelo grupo SofiaFala a qualquer momento.</p>
<p>10. Esta Licença será regida e interpretada de acordo com as leis brasileiras. Caso qualquer condição desta Licença seja considerada inválida, tal invalidez não afetará as demais condições aqui contidas, que continuarão válidas.</p>
<p>11. O foro competente para resolver qualquer disputa judicial entre as partes será o foro da Justiça Federal de São Paulo, nos termos do art. 109, I, da Constituição Federal, com exclusão de qualquer outro, por mais privilegiado que seja.</p>
<br />
<p>O USUÁRIO RECONHECE EXPRESSAMENTE QUE LEU AS CONDIÇÕES DESTA LICENÇA E COMPREENDEU OS DIREITOS E OBRIGAÇÕES NELA ESTABELECIDOS. AO CLICAR NO BOTÃO ACEITAR E/OU CONTINUAR A INSTALAR OU USAR O SOFTWARE, O USUÁRIO CONSENTE EM ESTAR OBRIGADO A CUMPRIR AS CONDIÇÕES DA LICENÇA.</p>


<br />
<br />
<br />

	<?php include_once("resources/template/footer.php"); ?>
</body>
</html>