<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}
$navbar = true;
?>

  <!DOCTYPE html>

  <html xmlns="http://www.w3.org/1999/xhtml">

  <head runat="server">
    <title>Projeto Sofia - An&#225;lise de Segmentos</title>
    <meta charset="utf-8">

    <?php include_once("resources/template/header.php"); ?>
    <?php include_once("resources/template/header-logado.php"); ?>

  </head>

  <body>

    <div class="container">
      <br />
      <br />
      <h1 class='text-center'>Bem-vindo</h1>
      <h3 class='text-center'>Prezado usuário, a equipe SofiaFala agradece sua participação. Para ter os melhores resultados, pedimos por gentileza você assista o seguinte video tutorial. Qualquer dúvida ou sugestão, pode escrever ao nosso email de contato: <a href="mailto:sofiafala.contato@gmail.com ">sofiafala.contato@gmail.com</a></h3>
      <br />
        <div class='row col-xs-12 col-sm-12 col-md-12 justify-content-center align-items-center'>
          <video id='video_meio' class='col-xs-8 col-sm-8 col-md-8' controls>
            <source id='fonteVideo' src="img/content/video_tutorial.mp4" type='video/mp4' />
          </video>
        </div>
      </div>
      </div>

    <?php include_once("resources/template/footer.php"); ?>
  </body>

  </html>
