######################################################
SofiaFala - README
Aplicativos: sofiaFala_paciente, sofiaFala_fono
######################################################

Autor: Ronen R. S. Filho
Data da criação: 05/11/2018
Data/hora atualização: 22/11/2018 1633

-----------------------------------------------------
# Modo de instalação: 

1) realize o download do app via o link: http://dcm.ffclrp.usp.br/sofiafala/extra/apk/sofiafala_paciente.apk

2) após a conclusão do download click no arquivo para iniciar a instalação.

3) o dispositivo pode pedir a autorização de "instalação de fontes desconhecidas",
caso isso aconteça, deve ser realizada a permissão.
O android vai fornecer instruções e o caminho para realizar a liberação da permissão. 

Material complementar: https://seletronic.com.br/noticias/android/sistema-android/como-instalar-aplicativos-de-fontes-desconhecidas-no-android-apk/

-----------------------------------------------------

# link's: 

README
http://dcm.ffclrp.usp.br/sofiafala/extra/apk/README.txt

## Versão de teste
http://dcm.ffclrp.usp.br/sofiafala/extra/apk/sofiafala_test.apk
http://dcm.ffclrp.usp.br/sofiafala/extra/apk/sofiafala_fono_test.apk

## Versão de produção
http://dcm.ffclrp.usp.br/sofiafala/extra/apk/sofiafala_fono.apk
http://dcm.ffclrp.usp.br/sofiafala/extra/apk/sofiafala.apk

## Versão de produção (google drive)
(19/11/2018 - 18:13) sofiaFala_v.1.0.0.apk - https://drive.google.com/open?id=1XdxXpnGWc9SsI7AXYwrw3hA9qYvauebw
(23/11/2018 - 11:06) sofiaFala_v.1.0.1_a.apk - https://drive.google.com/open?id=1U2ffOQ9suQ6LPNJAdrxtgD6J8PlGWPIk
(03/12/2018 - 12:00) sofiaFala_v.1.0.3.apk - https://drive.google.com/open?id=1xmkSLVLZ8LQam4FUhEziScUOeYCCmdTU
(06/12/2018 - 15:17) sofiaFala_v.1.0.4.apk - https://drive.google.com/open?id=1Syd3unVtgppOOEt5MPp_FP47EOVhXDdy


-----------------------------------------------------

# Usuário DEMO (produção): 

Login: demo@demo.com.br
Senha: DEMO2018

-----------------------------------------------------

# Versões:

## Versão de produção

# Paciente
- 20181114_2315: sofiafala_paciente_prod.apk
- 20181114_1005_sofiafala_paciente_prod.apk
- 20181119_1808_sofiafala_prod.apk
- 20181120_1703_sofiafala_prod.apk
- 20181203_1156_sofiaFala_v.1.0.3.apk
- 20181206_1517_sofiaFala_v.1.0.4.apk


# Fono
- 20181114_2256: sofiafala_fono_prod.apk
- 20181120_1711_sofiafala_fono_prod.apk
- 20181127_1149_sofiafala_fono_v.0.4.0__producao.apk


## Versão de teste

## Paciente
- 20181114_1005: sofiafala_paciente.apk
- 20181113_1742: sofiafala_paciente.apk
- 20181113_1448: sofiafala_paciente.apk
- 20181113_0006: sofiafala_paciente.apk
- 20181112_2218: sofiafala_paciente.apk
- 20181109_1827: sofiafala_paciente.apk
- 20181109_1607: sofiafala_paciente.apk
- 20181106_1217: sofiafala_paciente.apk
- 20181105_1609: sofiafala_paciente.apk
- 20181105_1456: sofiafala_paciente.apk
- 20181105_1120: sofiafala_paciente.apk
- 20181105_1050: sofiafala_paciente.apk

## Fono
- 20181112_2225: sofiafala_fono.apk
- 20181119_1042_sofiafala_fono.apk

-----------------------------------------------------



