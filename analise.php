<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}
$navbar = true;

include_once("resources/library/database.php");
include_once("resources/library/funcoes.php");

$funcao = new funcoes();

//Habilitar para debugar
ini_set('xdebug.halt_level', E_WARNING);


  echo "<!DOCTYPE html>";
  echo "<html xmlns='http://www.w3.org/1999/xhtml'>";

  echo "<head runat='server'>";
    echo "<title>Projeto Sofia - An&#225;lise de Segmentos</title>";
    echo "<meta charset='utf-8'>";

    include_once("resources/template/header.php");
    include_once("resources/template/header-logado.php");

    echo "<style>";
    echo ".vcenter {";
    echo "display: inline-block;";
    echo "vertical-align: middle;";
    echo "float: none;";
    echo "}";
    echo "</style>";
  echo "</head>";

  echo "<body>";

  if (isset($_GET["page"])) {
      $page = $_GET["page"];
  } else {
      if (!isset($page)) {
          //if ($funcao->is_postback($_SERVER['REQUEST_METHOD'], $_SERVER['HTTP_REFERER'], $_SERVER['SCRIPT_NAME'])) {
          $page=1;
          //}
      }
  };

  if (isset($_GET["t"])) {
      $tipo = $_GET["t"];
  }

  $records = 3; // altere aqui o numero de registros por pagina

  //Instancia o banco de dados
  $db = new database;

  $start_from = ($page-1) * $records;

  //Analisa registros órfãos
  $funcao->deleta_registros_orfaos($_SESSION['idusuario'], $tipo);

  //Pega o número total de registros
  $qry = $db->query("SELECT
                      COUNT(*) AS TOTAL
                    FROM TESTE_VIDEO AS TV
                    INNER JOIN TESTE_TIPO AS TP ON TV.ID_TIPO = TP.ID_TIPO
                    INNER JOIN TESTE_SEQUENCIA AS TS ON TV.ID_SEQUENCIA = TS.ID_SEQUENCIA
                    WHERE
                    ATIVO = true
                    AND TP.ID_TIPO = $tipo
                    AND ID_VIDEO NOT IN (SELECT ID_VIDEO FROM TESTE_RESULTADO WHERE ID_USUARIO = ". $_SESSION['idusuario'] .")
                    ");
                    //AND ID_VIDEO NOT IN (SELECT ID_VIDEO FROM TESTE_RESULTADO WHERE ID_USUARIO = ". $_SESSION['idusuario'] .");");
  $row_sql = $db->retornar_uma_linha($qry);

  // //Checa para ver se é PostBack, se não for, é a primeira vez acessando. Nesse caso checa de onde parou e continua
  // if (!$funcao->is_postback($_SERVER['REQUEST_METHOD'], $_SERVER['HTTP_REFERER'], $_SERVER['SCRIPT_NAME'])){
  //   $_funcao = $funcao->continuar_analise($records, $page, $start_from, $_SESSION['idusuario']);
  // }

  $total_records = $row_sql[0];
  $total_pages = ceil($total_records / $records);

    $sql = "SELECT ID_VIDEO,
                   VIDEO,
                   TS.SEQUENCIA,
                   TP.TIPO
                  FROM TESTE_VIDEO AS TV
                  INNER JOIN TESTE_TIPO AS TP ON TV.ID_TIPO = TP.ID_TIPO
                  INNER JOIN TESTE_SEQUENCIA AS TS ON TV.ID_SEQUENCIA = TS.ID_SEQUENCIA
                  WHERE ATIVO = TRUE
                    AND TP.ID_TIPO = $tipo
                    AND ID_VIDEO NOT IN
                      (SELECT ID_VIDEO
                       FROM TESTE_RESULTADO
                       WHERE ID_USUARIO = ". $_SESSION['idusuario'] .")
                  AND ID_VIDEO >= (SELECT ID_VIDEO
                  FROM TESTE_VIDEO AS TV
                  INNER JOIN TESTE_TIPO AS TP ON TV.ID_TIPO = TP.ID_TIPO
                  INNER JOIN TESTE_SEQUENCIA AS TS ON TV.ID_SEQUENCIA = TS.ID_SEQUENCIA
                  WHERE ATIVO = TRUE
                    AND TP.ID_TIPO = $tipo
                    AND TV.ID_SEQUENCIA = 1
                    AND ID_VIDEO NOT IN
                      (SELECT ID_VIDEO
                       FROM TESTE_RESULTADO
                       WHERE ID_USUARIO = ". $_SESSION['idusuario'] .")
                  ORDER BY RANDOM()
                  LIMIT 1)
                  ORDER BY TV.ID_VIDEO
                  FETCH NEXT 3 ROWS ONLY;";

        // //Checa para ver se é PostBack, se não for, é a primeira vez acessando. Nesse caso checa de onde parou e continua
        if (!$funcao->is_postback($_SERVER['REQUEST_METHOD'], $_SERVER['HTTP_REFERER'], $_SERVER['SCRIPT_NAME'])){
          // $_funcao = $funcao->continuar_analise($records, $page, $start_from, $_SESSION['idusuario']);

          $_SESSION['total_paginas'] = ceil($total_records / $records);
          $_SESSION['pagina_atual'] = 1;
        }else{
          $_SESSION['pagina_atual'] += 1;
        }

        $resultado = $db->query($sql);

        if ($resultado) {
            $linhas = $db->num_rows($resultado);

            //Pega o tipo do vídeo passado na querystring porque não vai entrar no laço todo esse texto
            if ($_GET['t'] == 1){
              $tipo_video = "Beijo";
            }else if ($_GET['t'] == 2){
              $tipo_video = "Estalo de Língua";
            }else if ($_GET['t'] == 3){
              $tipo_video = "Sopro";
            }


            //Nao existe mais analise para fazer
            if ($linhas == 0){
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "<div class='container'>";
              echo "<br />";
              echo "<br />";
              echo "<h1 class='text-center'>Olá, ". strstr($_SESSION['nome'], ' ', true) ."!</h1>";
              echo "<h1 class='text-center'>Você não possui mais nenhum teste de ". $tipo_video . " para realizar no momento!</h1>";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "</div>";
            }else{
              echo "<div class='container' style='text-align: center'>";
              echo "<br />";
              echo "<br />";
              echo "<h3>Selecione os movimentos que melhor representam os momentos de um <u><h2>" . $tipo_video . "</h2></u></h3>";
              echo "<dl>Dica: Pause o vídeo e mova a barra de tempo para uma melhor precisão!</dl>";
              echo "<br />";
              echo "</div>";
            }

            while ($linha = $db->fetch_array($resultado)) {
                $id_video = $linha['id_video'];
                $nome_video = $linha['video'];
                $tipo_movimento = $linha['sequencia'];
                $video_tipo = $linha['tipo'];

                echo "<div class='container-fluid'>";
                echo "<div class='row'>";
                echo "<div class='col-xs-5 col-sm-5 col-md-4 text-center'>";
                if (strtolower($tipo_movimento) != "meio") {
                    echo "<video id='video_". strtolower($tipo_movimento). "' class='col-xs-12 col-sm-12 col-md-12' style='display:none;' controls>";
                } else {
                    echo "<video id='video_meio' class='col-xs-12 col-sm-12 col-md-12' controls>";
                }
                echo "<source id='fonteVideo' src=" . $nome_video . " type='video/mp4'></source>";
                echo "</video>";
                echo "</div>";

                //Botões
                if (strtolower($tipo_movimento) == "inicio") {
                    echo "<div class='col-xs-1 col-sm-1 col-md-2 align-self-bottom text-center'>";
                } elseif (strtolower($tipo_movimento) == "meio") {
                    echo "<div class='col-xs-1 col-sm-1 col-md-2 align-self-center text-center'>";
                } else {
                    echo "<div class='col-xs-1 col-sm-1 col-md-2 align-self-top text-center'>";
                }
                echo "<h1 class='text-center'><b>" . $tipo_movimento . "</b></h1>";
                echo "<div class='btn-group btn-group' role='group' >";
                echo "<button  id='btnSalvar_". strtolower($tipo_movimento). "' class='btn btn-primary' type='button'>";
                echo "<span class='glyphicon glyphicon-camera selecionar-frame'></span>";
                echo "</button>";
                echo "<div class='checkbox text-middle'>";
                echo "<label class='text-left' style='font-size: 2em;' title='Selecione essa opção caso o vídeo não seja analisável!'>";
                echo "<input id='ckbAnalisavel_". strtolower($tipo_movimento). "' type='checkbox' value='' onclick='Analisavel();'>";
                echo "<span class='cr'><i class='cr-icon fa fa-check'></i></span>";
                echo "</label>";
                echo "</div>";
                echo "<button id='btnDeletar_". strtolower($tipo_movimento). "' class='btn btn-primary' type='button' disabled='true'>";
                echo "<span class='glyphicon glyphicon-trash selecionar-frame'></span>";
                echo "</button>";
                echo "</div>";
                echo "</div>";

                if (strtolower($tipo_movimento) == "inicio") {
                    echo "<div class='col-xs-5 col-sm-5 col-md-2 align-self-bottom'>";
                    echo "<h3 class='text-center'><b>Seleção:</b></h3>";
                } elseif (strtolower($tipo_movimento) == "meio") {
                    echo "<div class='col-xs-5 col-sm-5 col-md-2 align-self-center'>";
                } else {
                    echo "<div class='col-xs-5 col-sm-5 col-md-2 align-self-top'>";
                }
                echo "<canvas id='cnvFrameSelecionado_". strtolower($tipo_movimento). "' style='border:1px solid #d3d3d3;' class='col-xs-12 col-sm-12 col-md-12'>";
                echo "<thumbnail id='fotoSalvar' src='' />";
                echo "</div>";

                if (strtolower($tipo_movimento) == "inicio") {
                    echo "<div class='col-xs-1 col-sm-1 col-md-3 align-self-bottom'>";
                    echo "<h3 class='text-center'><b>Motivo: *</b></h3>";
                } elseif (strtolower($tipo_movimento) == "meio") {
                    echo "<div class='col-xs-1 col-sm-1 col-md-3 align-self-center'>";
                } else {
                    echo "<div class='col-xs-1 col-sm-1 col-md-3 align-self-top'>";
                }
                echo "<div class='form-group form-vertical w-100 p-3' role='group'>";
                echo "<textarea id='txtMotivo_". strtolower($tipo_movimento). "' class='form-control' name='txtMotivo' maxlength='200' required='true' style='border: 1px solid #d3d3d3;' onchange='ProximaPagina(this);'></textarea>";
                echo "</div>";
                echo "</div>";

                echo "</div>";
                echo "<br />";
                echo "</div>";
                echo "</div>";

                echo "<br />";
                echo "<br />";

                $phpself = $_SERVER['PHP_SELF'];

                $lastPage = $total_pages;

                if ($page == 1){
                  $proxima_pagina = $page;
                }else{
                  $proxima_pagina = ($page + 1);
                }
                $pagina_anterior = ($page - 1);
                $primeira_pagina = 1;
                $ultima_pagina = $total_pages;

                echo "<script>";
                echo "$(document).ready(function() {";

                echo "$('#btnSalvar_". strtolower($tipo_movimento). "').click(function() {";
                echo "var video = document.getElementById('video_meio');";
                echo "var _motivo = document.getElementById('txtMotivo_". strtolower($tipo_movimento). "').value;";

                echo "if (_motivo.replace(/\s/g, '') === '') {";
                  echo "alert('Motivo deve ser preenchido!');";
                  echo "return;";
                echo "}";

                echo "$.ajax({";
                  echo "url: 'resources/library/salvar-imagem.php',";
                  echo "type: 'POST',";
                  echo "data: {";
                    echo "id_usuario: ". json_encode($_SESSION['idusuario'], JSON_HEX_TAG) .",";
                    echo "usuario: ". json_encode($_SESSION['usuario'], JSON_HEX_TAG) .",";
                    echo "tipo_movimento: ". json_encode($tipo_movimento, JSON_HEX_TAG) .",";
                    echo "nome_video: " . json_encode($nome_video, JSON_HEX_TAG) . ",";
                    echo "id_video: " . json_encode($id_video, JSON_HEX_TAG) . ",";
                    echo "tipo_video: " . json_encode($video_tipo, JSON_HEX_TAG) . ",";
                    echo "motivo: _motivo,";
                    echo "timestamp: video.currentTime";
                  echo "},";
                  echo "});";
                echo "});";

                echo "$('#btnDeletar_". strtolower($tipo_movimento). "').click(function() {";
                echo "$.ajax({";
                  echo "url: 'resources/library/salvar-imagem.php',";
                  echo "type: 'POST',";
                  echo "data: {";
                    echo "deletar: true,";
                    echo "id_usuario: ". json_encode($_SESSION['idusuario'], JSON_HEX_TAG) .",";
                    echo "id_video: " . json_encode($id_video, JSON_HEX_TAG) . ",";
                    echo "},";
                  echo "});";
                echo "});";

                echo "function isCanvasBlank(canvas) {";
                  echo "var blank = document.createElement('canvas');";
                  echo "blank.width = canvas.width;";
                  echo "blank.height = canvas.height;";

                  echo "return canvas.toDataURL() == blank.toDataURL();";
                echo "}";

                echo "function Salvar() {";
                echo "var _motivo = document.getElementById('txtMotivo_". strtolower($tipo_movimento). "').value;";

                echo "if (_motivo.replace(/\s/g, '') === '') {";
                  echo "return;";
                echo "}";

                echo "var video = document.getElementById('video_meio');";
                echo "var canvas = document.getElementById('cnvFrameSelecionado_". strtolower($tipo_movimento). "');";
                echo "var ctx = canvas.getContext('2d');";

                echo "var wRatio = video.videoWidth / canvas.width;";
                echo "var hRatio = video.videoHeight / canvas.height;";
                echo "var ratio = Math.min(wRatio, hRatio);";

                //Checa se o analisavel esta selecionado. Se estava, tem que tirar
                echo "var _analisavel = document.getElementById('ckbAnalisavel_". strtolower($tipo_movimento). "').checked;";
                echo "if (_analisavel){";
                  echo "document.getElementById('ckbAnalisavel_". strtolower($tipo_movimento). "').checked = false;";
                echo "}";

                //Habilita o botão de deletar
                echo "document.getElementById('btnDeletar_". strtolower($tipo_movimento). "').disabled = false;";

                echo "ctx.drawImage(video, 0, 0, canvas.width, canvas.height);";

                echo "ProximaPagina(this);";
                echo "}";

                echo "function Deletar() {";
                echo "var canvas = document.getElementById('cnvFrameSelecionado_". strtolower($tipo_movimento). "');";
                  echo "if (!isCanvasBlank(canvas)){";
                    echo "document.getElementById('btnDeletar_". strtolower($tipo_movimento). "').disabled = true;";
                    echo "document.getElementById('txtMotivo_". strtolower($tipo_movimento). "').value = '';";
                    echo "var ctx = canvas.getContext('2d');";
                    echo "ctx.clearRect(0, 0, canvas.width, canvas.height);";
                  echo "}";
                  echo "ProximaPagina(this);";
                echo "}";


                echo "function Analisavel() {";
                echo "var analisavel = document.getElementById('ckbAnalisavel_". strtolower($tipo_movimento). "').checked;";

                // echo "if (!analisavel) {";
                //   echo "document.getElementById('ckbAnalisavel_". strtolower($tipo_movimento). "').checked = true;";
                // echo "}else {";
                //   echo "document.getElementById('ckbAnalisavel_". strtolower($tipo_movimento). "').checked = false;";
                // echo "}";

                echo "if (analisavel){";
                  echo "var video = document.getElementById('video_meio');";
                  echo "var _motivo = document.getElementById('txtMotivo_". strtolower($tipo_movimento). "').value;";

                  echo "if (_motivo.replace(/\s/g, '') === '') {";
                    echo "alert('Motivo deve ser preenchido!');";
                    // echo "document.getElementById('ckbAnalisavel_". strtolower($tipo_movimento). "').checked = false;";
                    echo "document.getElementById('ckbAnalisavel_". strtolower($tipo_movimento). "').checked = false;";
                    echo "return;";
                  echo "}";

                  echo "$.ajax({";
                    echo "url: 'resources/library/salvar-imagem.php',";
                    echo "type: 'POST',";
                    echo "data: {";
                      echo "id_usuario: ". json_encode($_SESSION['idusuario'], JSON_HEX_TAG) .",";
                      echo "usuario: ". json_encode($_SESSION['usuario'], JSON_HEX_TAG) .",";
                      echo "tipo_movimento: ". json_encode($tipo_movimento, JSON_HEX_TAG) .",";
                      echo "nome_video: " . json_encode($nome_video, JSON_HEX_TAG) . ",";
                      echo "id_video: " . json_encode($id_video, JSON_HEX_TAG) . ",";
                      echo "tipo_video: " . json_encode($video_tipo, JSON_HEX_TAG) . ",";
                      echo "motivo: _motivo,";
                      echo "timestamp: video.currentTime,";
                      echo "analisavel: false";
                    echo "},";
                  echo "});";

                  echo "var canvas = document.getElementById('cnvFrameSelecionado_". strtolower($tipo_movimento). "');";
                  echo "if (!isCanvasBlank(canvas)) {";
                    echo "document.getElementById('btnDeletar_". strtolower($tipo_movimento). "').disabled = true;";
                    echo "document.getElementById('btnDeletar_". strtolower($tipo_movimento). "').click();";
                    echo "document.getElementById('txtMotivo_". strtolower($tipo_movimento). "').value = '';";
                    echo "var ctx = canvas.getContext('2d');";
                    echo "ctx.clearRect(0, 0, canvas.width, canvas.height);";
                  echo "}";
                  echo "}";

                  echo "ProximaPagina(this);";
                echo "}";

                echo "btnSalvar_". strtolower($tipo_movimento). ".addEventListener('click', Salvar, false);";
                echo "btnDeletar_". strtolower($tipo_movimento). ".addEventListener('click', Deletar, false);";
                echo "ckbAnalisavel_". strtolower($tipo_movimento). ".addEventListener('click', Analisavel, false);";

                echo "});";

                echo "function CanvasVazio(canvas) {";
                  echo "var blank = document.createElement('canvas');";
                  echo "blank.width = canvas.width;";
                  echo "blank.height = canvas.height;";

                  echo "return canvas.toDataURL() == blank.toDataURL();";
                echo "}";

                echo "function ProximaPagina(who_called) {";
                  // echo "event.preventDefault();"; // Não habilitar essa linha. Dá pau no Firefox.
                  echo "var movimentos = ['inicio', 'meio', 'fim'];";
                  echo "var botao = document.getElementById('btnProximaPagina').disabled;";
                  echo "validacao = true;";

                  echo "for (i=0; i < movimentos.length; i++) {";
                    echo "var frame = document.getElementById('cnvFrameSelecionado_' + movimentos[i]);";
                    echo "var motivo = document.getElementById('txtMotivo_' + movimentos[i]).value;";
                    echo "var analisavel = document.getElementById('ckbAnalisavel_' + movimentos[i]).checked;";

                    echo "if (CanvasVazio(frame) && analisavel === false) {";
                      // echo "alert('Favor selecionar um frame ou marcar como não analisável o movimento: ' + movimentos[i]);";
                      echo "validacao = false;";
                    echo "}else if (!CanvasVazio(frame) && motivo.replace(/\s/g, '') === '') {";
                      // echo "alert('Favor informar o motivo do movimento: ' + movimentos[i]);";
                      echo "validacao = false;";
                    echo "}else if (analisavel === true && motivo.replace(/\s/g, '') === '') {";
                      // echo "alert('Favor informar o motivo do vídeo não poder ser analisado no movimento: ' + movimentos[i]);";
                      echo "validacao = false;";
                    echo "}";

                    echo "if (!validacao && !botao) {";
                      echo "document.getElementById('btnProximaPagina').disabled = true;";
                      echo "return;";
                    echo "}else if (!validacao) {";
                      echo "return;";
                    echo "}";

                  echo "}";

                  echo "if (validacao) {";
                      echo "document.getElementById('btnProximaPagina').disabled = false;";
                  echo "}";

                  //Se foi o botao ProximaPagina quem chamou, pode ir, pois ele ja foi verificado
                  echo "if (who_called.id.includes('btnProximaPagina')) {";
                    echo "window.location = '". $phpself ."?page=".$proxima_pagina."&t=".$tipo."';";
                  echo "}";

                echo "}";

                echo "</script>";

            }

            //BOTÕES INFERIORES - Anterior e Próximo
            echo "<div class='container text-center vcenter' style='text-align: center'>";
            // if ($page >= 1)
            // {
            //   echo "<button class='btn btn-primary' type='button' name='btnPaginaAnterior' onclick='location.href=\"$phpself?page=$pagina_anterior&t=$tipo\"'>Anterior</button>";
            //   echo "&nbsp;";
            // }
            //
            // echo "&nbsp;";

            //ARRUMAR
            if ($page < $ultima_pagina) {
              // echo "<h3 class='text-center'><b>Vídeo " . $_SESSION['pagina_atual'] ." de ". $_SESSION['total_paginas'] ."</b></h3>";
              echo "<h3 class='text-center'><b>Faltam " . $row_sql[0]/3 ." vídeos.</b></h3>";

              echo "<br />";
              echo "<button class='btn btn-primary' type='button' id='btnProximaPagina' name='btnProximaPagina' onclick='ProximaPagina(this);' disabled title='Complete todos os movimentos para avançar!'>Próxima</button>";
              echo "&nbsp;";
            }

            if ($page == $ultima_pagina) {
                echo "<div class='container text-center vcenter' style='text-align: center'>";
                echo "<script>";
                echo "document.getElementById('btnProximaPagina').style.display = 'none';";
                echo "</script>";
                echo "<button class='btn btn-primary' type='button' id='btnFinal' name='btnFinal' onclick='location.href=\"final-teste.php?t=$tipo\"'>Finalizar Teste</button>";
                echo "</div>";
            }
            echo "</div>";
        }
        $db->close();

  include_once("resources/template/footer.php");



  echo "</body>";
  echo "</html>";
