<?php
session_start();
if(!isset($_SESSION['usuario'])){
   header("Location:index.php");
}
$navbar = true;

include_once("funcoes.php");
?>

  <!DOCTYPE html>

  <html xmlns="http://www.w3.org/1999/xhtml">

  <head runat="server">
    <title>Projeto Sofia - An&#225;lise de Segmentos</title>
    <meta charset="utf-8">

    <?php include_once("resources/template/header.php"); ?>
    <?php include_once("resources/template/header-logado.php"); ?>

  </head>

  <body>

    <div class="container">
      <?php
        $fotos = glob("img/resultados/*".$_SESSION['usuario']."*");
        echo "<br />";
        echo "<br />";
        echo "<br />";
        foreach($fotos as $foto)
        {
          echo "<a href=$foto>".basename($foto)."</a>";
          echo "<br />";
        }
       ?>
    </div>

    <?php include_once("resources/template/footer.php"); ?>
  </body>

  </html>
