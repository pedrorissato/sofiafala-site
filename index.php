<?php
include('resources/library/database.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <title>SofiaFala - Software Inteligente de Apoio &agrave; Fala</title>
    <?php include_once("resources/template/header.php"); ?>
  </head>
  <body>
    <div class="container">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
          <div class="item active">
            <img src="img/content/slider1.jpg" style="width:100%;">
          </div>
          <div class="item">
            <img src="img/content/slider2.jpg" style="width:100%;">
          </div>
          <div class="item">
            <img src="img/content/slider3.jpg" style="width:100%;">
          </div>
          <div class="item">
            <img src="img/content/slider6.jpg" style="width:100%;">
          </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Anterior</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Próximo</span>
        </a>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="text-center">
            <h2>Bem vindo ao SofiaFala</h2>
            <p>O Projeto SofiaFala é uma tecnologia assistiva (capaz de melhorar qualidade de vida de pessoas com dificuldades de fala) que capta sons e imagens, produzidos durante a execução do exercício fonoaudiológico, e depois os analisa, oferecendo dois tipos de respostas sobre a performance do usuário: uma lúdico-educacional, com orientações para o paciente e/ou o responsável pelo treino; outra, com dados métricos e estatísticos para o Fonoaudiólogo avaliar, acompanhar e orientar a evolução clínica do usuário. Assim, o sistema deverá se adaptar às características do usuário para melhor conduzi-lo no processo de evolução da fala, isto é, o sistema terá comportamentos distintos com usuários diferentes devido à singularidade, dificuldade e potencialidade de cada um, e seu uso deverá ser indicado e acompanhado por um fonoaudiólogo clínico como apoio ao processo de intervenção terapêutica.</p>
          </div>
          <hr>
        </div>
      </div>
    </div>
    <div class="container" id="sobre">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="text-center">
            <h2>Sobre o SofiaFala</h2>
            <p>Quando a Sofia nasceu, sua mãe, a cientista da computação Marinalva Dias Soares se deu conta de que necessitaria de recursos para ajudá-la. Sofia foi diagnosticada com síndrome de Down ao nascer e precisaria de tratamento fonoaudiológico  (entre outras terapias) para desenvolver a fala e Marinalva pretendia incrementar essa terapia em sua própria casa. Como as tecnologias existentes não atendiam as necessidades da Sofia, as duas cientistas da computação e seus colegas decidiram criar uma tecnologia mais adequada à realidade das famílias brasileirasFoi então que a pesquisadora Marinalva, mãe da Sofia, procurou a professora Alessandra na FFCLRP.
            Com o desenvolvimento do projeto, a expectativa é que a Sofia e outras crianças possam aprimorar a comunicação verbal, por meio do uso do sistema, e assim possam falar com fluncia e segurança para poderem integrar e interagir no contexto familiar, educacional e social exercendo seus direitos e deveres como qualquer cidadão brasileiro. O nome do sistema, SofiaFala, foi criado pela coordenadora deste projeto em homenagem à querida Sofia.</p>
          </div>
          <hr>
        </div>
      </div>
    </div>
    <div class="container" id="pesquisa">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="text-center">
            <h2>Pesquisas relacionadas</h2>
            <p>A Síndrome de Down é um acidente genético ocorrido na divisão celular, o que pode ocorrer em qualquer gestação, independente de raça, etnia, condição social e idade. Essa síndrome não é uma doença e não é progressiva, ou seja, não se agrava
              com o tempo. É conhecido que as chances de se ter um filho com SD se elevam em mulheres acima de 35 anos, mas o mesmo acontece, inexplicavelmente, em mulheres abaixo dos 30 anos. A SD caracteriza-se por sinais físicos peculiares sendo a
              deficiência intelectual, a dificuldade na fala e a hipotonia os sintomas mais comuns.<br /> As pessoas com síndrome de Down têm muito mais em comum com o resto da população do que diferenças. Para inserção da pessoa com SD na sociedade,
              enquanto cidadão, a deficiência intelectual e as dificuldades de linguagem oral (verbal) não devem ser considerados um empecilho para o exercício desse direito, portanto a intervenção precoce e durante todo o ciclo vital é necessário, pois
              contribuirá para o desenvolvimento de habilidades e competências para a aprendizagem global, consideradas, respeitando a individualidade, o potencial produtivo e a capacidade laborativa das pessoas com SD. Porém, para que essas limitações
              não sejam impeditivas para o desenvolvimento, o aprendizado e a participação efetiva da pessoa com SD na sociedade, a comunicação oral e escrita tornam-se o elemento-chave. <br /> A criança tem a capacidade de sentir, amar, aprender, se
              divertir e trabalhar. Poderá ler e escrever, deverá ir à escola como qualquer outra criança e levar uma vida autônoma. Em resumo, ela poderá ocupar um lugar próprio e digno na sociedade (Fonte: APAE Rio do Sul). Diversas ONGs e instituições
              nacionais e internacionais tem investido esforços e apoiado pessoas com deficiência intelectual e suas famílias. Dentre as quais é importante destacar:<br /><br />
              <strong>Nacionais:</strong><br /><br />
              <a href="http://www.fsdown.org.br/" target="_blank">http://www.fsdown.org.br/</a><br />
              <a href="http://www.movimentodown.org.br/" target="_blank">http://www.movimentodown.org.br/</a><br />
              <a href="https://reviverdown.org.br" target="_blank">https://reviverdown.org.br</a><br />
              <a href="http://www.federacaodown.org.br/portal/" target="_blank">http://www.federacaodown.org.br/portal/</a><br />
              <a href="http://www.adid.org.br;" target="_blank">http://www.movimentodown.org.br/</a><br />
              <a href="http://www.ceesd.org.br" target="_blank">http://www.ceesd.org.br</a><br /><br />
              <strong>Internacionais:</strong><br /><br />
              <a href="http://www.nads.org/" target="_blank">http://www.nads.org/</a><br />
              <a href="http://www.globaldownsyndrome.org/" target="_blank">http://www.globaldownsyndrome.org/</a><br />
              <a href="https://ds-int.org" target="_blank">https://ds-int.org</a><br />
              <a href="http://www.dsrf.org/about-us/" target="_blank">http://www.dsrf.org/about-us/</a><br />
              <a href="http://cdss.ca/" target="_blank">http://cdss.ca/</a><br />
              <a href="https://www.downs-syndrome.org.uk" target="_blank">https://www.downs-syndrome.org.uk</a><br />
              <a href="https://www.downsyndromefoundation.org/" target="_blank">https://www.downsyndromefoundation.org/</a>
            </p>
          </div>
          <hr>
        </div>
      </div>
    </div>
    
    <div class="container" id="demonstracao">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="text-center">
            <h2>Demonstração</h2>
          </div>
          <hr>
        </div>
      </div>
    </div>
    <div class="container" id="videos-demonstracao">
      <div class="row">
        
        <div class="col-md-6 col-md-offset-3">
          <div class="text-center">
            <h3>Fonoaudiólogo(a)</h3>
          </div>
        </div>
        <div class="container col-md-offset-1" id="demonstracaoFono">
          <video id='demo_sofiafala_fono' class='col-xs-10 col-sm-10 col-md-10' controls>
            <source id='fonteVideoFono' src="img/content/sofiafala-demo-fono.mp4" type='video/mp4' />
          </video>
        </div>
        <div class="col-md-6 col-md-offset-3" id="demonstracaoPaciente">
          <div class="text-center">
            <h3>Paciente</h3>
          </div>
        </div>
        <div class="container col-md-offset-1">
          <video id='demo_sofiafala_usuario' class='col-xs-10 col-sm-10 col-md-10' controls>
            <source id='fonteVideoUsuario' src="img/content/sofiafala-demo-usuario.mp4" type='video/mp4' />
          </video>
        </div>
      </div>
    </div>
    <br />
    <div class="container" id="imprensa">
      <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
          <h2>Imprensa</h2>
          <ul class="cards">
            <li class="cards__item">
              <div class="noticias-card">
                <div class="card__image" style="background-image:url(img/content/logo_USP.jpg)">
                </div>
                <div class="card__content">
                  <div class="card__title">Jornal da USP</div>
                  <p class="card__text"><a href="https://jornal.usp.br/?p=223525" target="_blank" style="color: #000000">Aplicativo desenvolvido na USP ajuda a treinar fala de crianças com Down.
                  <br> Foto: Reprodução / Portal da USP.</a></p>
                </div>
              </div>
            </li>
            <li class="cards__item">
              <div class="noticias-card">
                <div class="card__image" style="background-image:url(img/content/logo_R7.png)"></div>
                <div class="card__content">
                  <div class="card__title">Programa Fala Brasil</div>
                  <p class="card__text"><a href="http://recordtv.r7.com/fala-brasil/videos/aplicativo-de-celular-estimula-interacao-e-fala-de-criancas-com-sindrome-de-down-16022019" target="_blank" style="color: #000000">Aplicativo de celular estimula interação e fala de crianças com síndrome de down.
                  <br>Foto: Reprodução / Portal R7.</a></p>
                </div>
              </div>
            </li>
            <li class="cards__item">
              <div class="noticias-card">
                <div class="card__image" style="background-image:url(img/content/logo_G1.png)">
                </div>
                <div class="card__content">
                  <div class="card__title">EPTV</div>
                  <p class="card__text"><a href="https://g1.globo.com/sp/ribeirao-preto-franca/noticia/2019/03/07/aplicativo-desenvolvido-em-ribeirao-preto-para-criancas-com-down-analisa-e-corrige-a-fala.ghtml" target="_blank" style="color: #000000">Aplicativo desenvolvido em Ribeirão Preto para crianças com Down analisa e corrige a fala.
                  <br> Foto: Reprodução / Portal G1.</a></p>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
          <ul class="cards">
            <li class="cards__item">
              <div class="noticias-card">
                <div class="card__image" style="background-image:url(img/content/logo_Tecmundo.jpg)">
                </div>
                <div class="card__content">
                  <div class="card__title">Tecmundo</div>
                  <p class="card__text"><a href="https://www.tecmundo.com.br/software/139547-aplicativo-facilita-comunicacao-criancas-sindrome-down.htm" target="_blank" style="color: #000000">Aplicativo facilita a comunicação de crianças com síndrome de Down.
                  <br> Foto: Reprodução / Tecmundo.</a></p>
                </div>
              </div>
            </li>
            <li class="cards__item">
              <div class="noticias-card">
                <div class="card__image" style="background-image:url(img/content/logo_GovernoSP.png)"></div>
                <div class="card__content">
                  <div class="card__title">SP Notícias</div>
                  <p class="card__text"><a href="http://www.saopaulo.sp.gov.br/spnoticias/aplicativo-ajuda-a-treinar-fala-de-criancas-com-sindrome-de-down/" target="_blank" style="color: #000000">Aplicativo ajuda a treinar fala de crianças com Síndrome de Down.
                  <br>Foto: Reprodução / Governo do Estado de São Paulo.</a></p>
                </div>
              </div>
            </li>
            <li class="cards__item">
              <div class="noticias-card">
                <div class="card__image" style="background-image:url(img/content/logo_Crescer.jpg)">
                </div>
                <div class="card__content">
                  <div class="card__title">Revista Crescer</div>
                  <p class="card__text"><a href="" target="_blank" style="color: #000000">App brasileiro promete ajudar crianças com síndrome de Down no desenvolvimento da fala.
                  <br> Foto: Reprodução / Revista Crecer.</a></p>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="container" id="membros">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="text-center">
            <h2>Membros</h2>
          </div>
          <hr>
        </div>
      </div>
    </div>
    <div class="container" id="area-concentracao-coordenacao">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="text-center">
            <h3>Coordenação</h3>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Alessandra_Alaniz.jpg" alt="Alessandra_Alaniz" />
              <figcaption>
              <h4>Prof&ordf; Dr&ordf;<br /><span>Alessandra Alaniz<br /> Macedo</span></h4>
              <a href="http://lattes.cnpq.br/2407277993285186" target="_blank">
              </a>
              <p class="description"><b>Coordenadora do projeto</b><br/>Extra&ccedil;&atilde;o de Informa&ccedil;&atilde;o<br />FFCLRP – USP</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Patricia_Mandra.jpg" alt="Patricia_Mandra" />
              <figcaption>
              <h4>Prof&ordf; Dr&ordf;<br /><span>Patricia P. Mandrá</span></h4>
              <a href="http://lattes.cnpq.br/0726260574298864" target="_blank">
              </a>
              <p class="description"><b>Docente Curso Fonoaudiologia</b><br/>Transtornos da linguagem e fala<br /> na inf&acirc;ncia, FMRP - USP</p>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </div>
    <br >
    <br >
    <div class="container" id="area-concentracao-computacao">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="text-center">
            <h3>Computação</h3>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Felipe_Cabrera_Ribeiro_dos_Santos.jpg" alt="Felipe Cabrera" />
              <figcaption>
              <h4><span>Felipe Cabrera Ribeiro <br />dos Santos</span></h4>
              <a href="http://lattes.cnpq.br/8398808056185881" target="_blank">
              </a>
              <p class="description"><b>Desenvolvimento de Software <br/> Engenharia de Software</b><br/>FFCLRP - DCM - USP</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Fernando_Meloni.jpg" alt="Fernando Meloni" />
              <figcaption>
              <h4>Dr&ordm;<br /><span>Fernando Meloni</span></h4>
              <a href="http://lattes.cnpq.br/9143424316420654" target="_blank">
              </a>
              <p class="description"><b>Desenvolvimento de Software <br/> Modelagem de Sistemas Complexos</b><br/>FFCLRP - DCM - USP</p>
              </figcaption>
            </figure>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Jose_Augusto_Baranauskas.jpg" alt="José Augusto Baranauskas" />
              <figcaption>
              <h4>Prof&ordm; Dr&ordm;<br /><span>José Augusto Baranauskas</span></h4>
              <a href="http://lattes.cnpq.br/4050202318600269" target="_blank">
              </a>
              <p class="description"><b>Classificação de Dados</b><br/>Pesquisador Colaborador, USP</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Luiz_Otavio_Murta.jpg" alt="Luiz Otávio Murta" />
              <figcaption>
              <h4>Prof&ordm; Dr&ordm;<br /><span>Luiz Otavio Murta</span></h4>
              <a href="http://lattes.cnpq.br/0276583403489241" target="_blank">
              </a>
              <p class="description"><b>Processamento de Sinais e Imagem</b><br/>Pesquisador Colaborador, USP</p>
              </figcaption>
            </figure>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Marinalva_D_Soares.jpg" alt="Marinalva D. Soares" />
              <figcaption>
              <h4>Prof&ordf; Dr&ordf;<br /><span>Marinalva D. Soares</span></h4>
              <a href="http://lattes.cnpq.br/6047184744431931" target="_blank">
              </a>
              <p class="description"><b>Reconhecimento de padr&otilde;es e<br/> processamento de imagens</b><br/>Pesquisadora – INPE</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Pedro_Henrique_DAlmeida_Giberti_Rissato.jpg" alt="Pedro Henrique D'Almeida Giberti Rissato" />
              <figcaption>
              <h4><span>Pedro Henrique D'Almeida<br />Giberti Rissato</span></h4>
              <a href="http://lattes.cnpq.br/9400608160057217" target="_blank">
              </a>
              <p class="description"><b>Reconhecimento de padr&otilde;es e<br/> processamento de imagens</b><br/>FFCLRP - DCM - USP</p>
              </figcaption>
            </figure>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Renato_de_Freitas_Bulcao_Neto.jpg" alt="Renato de Freitas Bulcão Neto" />
              <figcaption>
              <h4>Prof&ordm; Dr&ordm;<br /><span>Renato de Freitas Bulcão<br />Neto</span></h4>
              <a href="http://lattes.cnpq.br/5627556088346425" target="_blank">
              </a>
              <p class="description"><b>Desenvolvimento de Software e<br/> Engenharia de Software</b><br/>INF - UFG</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Renato_Tinos.jpg" alt="Renato Tinós" />
              <figcaption>
              <h4>Prof&ordm; Dr&ordm;<br /><span>Renato Tinós</span></h4>
              <a href="http://lattes.cnpq.br/1273134370963830" target="_blank">
              </a>
              <p class="description"><b>Redes Neurais Artificiais</b><br/>Pesquisador Colaborador, USP</p>
              </figcaption>
            </figure>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Ronen_Rodrigues_Silva_Filho.jpg" alt="Ronen Rodrigues Silva Filho" />
              <figcaption>
              <h4><span>Ronen Rodrigues Silva Filho</span></h4>
              <a href="http://lattes.cnpq.br/5476199318915275" target="_blank">
              </a>
              <p class="description"><b>Desenvolvimento de Software <br/>Computação Aplicada à Saúde & Sistemas Complexos de Informação</b><br/>FFCLRP - DCM - USP</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Vinicius_Souza_Goncalves.jpg" alt="Vinicius Gonçalves" />
              <figcaption>
              <h4><span>Vinícius de Souza Gonçalves</span></h4>
              <a href="http://lattes.cnpq.br/4740606840150876" target="_blank">
              </a>
              <p class="description"><b>Interação humano-computador</b><br/>FFCLRP - DCM - USP</p>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </div>
    <br >
    <br >
    <div class="container" id="area-concentracao-fonoaudiologia">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="text-center">
            <h3>Fonoaudiologia</h3>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Bianca_Bortolai_Sicchieri.jpg" alt="Bianca Bortolai Sicchieri" />
              <figcaption>
              <h4><span>Bianca Bortolai Sicchieri</span></h4>
              <a href="http://lattes.cnpq.br/8469953057186355" target="_blank">
              </a>
              <p class="description"><b>Fonoaudióloga <br/>Bolsista CNPq</b><br/>FFCLRP - DCM - USP</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Maria_Roberta_Dias_Veneziani_Cantarelli.jpg" alt="Maria Roberta Dias Veneziani Cantarelli" />
              <figcaption>
              <h4>
              <!--Prof&ordf; Dr&ordf;<br />--><span>Maria Roberta Dias<br />Veneziani Cantarelli</span>
              </h4>
              <a href="http://lattes.cnpq.br/9166118554622109" target="_blank">
              </a>
              <p class="description"><b>Fonoaudiologia e Equoterapia</b><br/>Fonoaudióloga na clínica M&W Reabilitação</p>
              </figcaption>
            </figure>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Myrian_Christina_Neves.jpg" alt="Myrian Christina Neves" />
              <figcaption>
              <h4><span>Myrian Christina Neves<br /> Botelho de Andrade</span></h4>
              <a href="http://lattes.cnpq.br/1732160193750270" target="_blank">
              </a>
              <p class="description"><b>Fonoaudiologia</b><br/>Fonoaudióloga no Centro Terapêutico Vale Crescer em São José dos Campos - SP</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Thais_Moretti.jpg" alt="Thaís Moretti" />
              <figcaption>
              <h4><span>Thaís Moretti</span></h4>
              <a href="http://lattes.cnpq.br/5671857580042237" target="_blank">
              </a>
              <p class="description"><b>Fonoaudiologia</b><br/>Fonoaudióloga no Centro Integrado de Reabilitação (CIR)</p>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </div>
    <br >
    <br >
    <div class="container" id="area-concentracao-colaboradores-multidisciplinares">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="text-center">
            <h3>Colaboradores Multidisciplinares</h3>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Andrea_Leao_Doescher.jpg" alt="Andréa Leão Doescher" />
              <figcaption>
              <h4><br /><span>Andréa M. Leão Doescher</span></h4>
              <a href="http://lattes.cnpq.br/0440854881429269" target="_blank">
              </a>
              <p class="description"><b>Epidemiologia</b><br/>Psicóloga Doutoranda - UNIFESP</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Aline_Saraiva.jpg" alt="Aline Saraiva Guerreiro Camargo " />
              <figcaption>
              <h4><span>Aline Saraiva Guerreiro<br /> Camargo </span></h4>
              <a href="http://lattes.cnpq.br/2315626804304828" target="_blank">
              </a>
              <p class="description"><b>Psicologia Cognitiva</b><br/>Psicóloga e Especialista em Psicoterapia Cognitiva<br/> na Clinica Nosso Espaço</p>
              </figcaption>
            </figure>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Alessandra_Yoshida.jpg" alt="Alessandra Yoshida " />
              <figcaption>
              <h4><br />
              <span>Alessandra Yoshida </span>
              </h4>
              <a href="http://lattes.cnpq.br/4360147174147661" target="_blank">
              </a>
              <p class="description"><b>Fisioterapia e Terapia Ocupacional</b><br/>Terapeuta Ocupacional no<br/> Centro Terapêutico Vale Crescer</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Carolina_Watanabe.jpg" alt="Carolina Yukari Veludo Watanabe" />
              <figcaption>
              <h4>Prof&ordf; Dr&ordf;<br /><span>Carolina Yukari<br/> Veludo Watanabe</span></h4>
              <a href="http://lattes.cnpq.br/5070373341032103" target="_blank">
              </a>
              <p class="description"><b>Processamento Gráfico</b><br/>DI – UNIR</p>
              </figcaption>
            </figure>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Danielle_Magalhaes_Moraes.jpg" alt="Danielle Magalhães Moraes" />
              <figcaption>
              <h4>Dr&ordf;<br /><span>Danielle Magalhães Moraes</span></h4>
              <a href="http://lattes.cnpq.br/7793443233826191" target="_blank">
              </a>
              <p class="description"><b>Sensores</b><br/>Pesquisadora ITA</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/layout/membro_bck.jpg" alt="membro_bck" />
              <figcaption>
              <h4>Dr&ordm;<br /><span>Gilberto Medeiros Nakamura</span></h4>
              <a href="http://lattes.cnpq.br/9758892425695434" target="_blank">
              </a>
              <p class="description"><b>Física Estatística e Termodinâmica</b><br/>DCM – USP</p>
              </figcaption>
            </figure>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Maraisa_Helena_Borges_Estevao_Pereira.jpg" alt="Maraísa Helena Borges Estêvão Pereira" />
              <figcaption>
              <h4><span>Maraísa Helena Borges<br /> Estêvão Pereira</span></h4>
              <a href="http://lattes.cnpq.br/9768757387259360" target="_blank">
              </a>
              <p class="description"><b>Doutoranda em Ciencia de<br/>la Educación, Humanidad y Artes</b><br/>Especialista em neuropsicopedagogia</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Vilson_Rosa_Almeida.jpg" alt="Vilson R. de Almeida " />
              <figcaption>
              <h4>Prof&ordm; Dr&ordm;<br /><span>Vilson R. de Almeida </span></h4>
              <a href="http://lattes.cnpq.br/2520874965506616" target="_blank">
              </a>
              <p class="description"><b>Engenharia Biomédica</b><br/>Pesquisador ITA</p>
              </figcaption>
            </figure>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Zan_Mustacchi.jpg" alt="Zan Mustacchi" />
              <figcaption>
              <h4>Dr&ordm;<br /><span>Zan Mustacchi</span></h4>
              <a href="http://lattes.cnpq.br/4419319234242523" target="_blank">
              </a>
              <p class="description"><b>Geneticista especialista em<br/> síndrome de Down</b><br/>Médico Geneticista e Pediatra - USP</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala" style="visibility: hidden;">
              <img src="img/content/membros/membro_Zan_Mustacchi.jpg" alt="Zan Mustacchi" />
              <figcaption>
              <h4>Dr&ordm;<br /><span>Zan Mustacchi</span></h4>
              <a href="http://lattes.cnpq.br/4419319234242523" target="_blank">
              </a>
              <p class="description"><b>Geneticista especialista em<br/> síndrome de Down</b><br/>Médico Geneticista e Pediatra - USP</p>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </div>
    <br >
    <br >
    <div class="container" id="area-concentracao-ex-bolsistas">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="text-center">
            <h3>Ex-bolsistas</h3>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Alinne_Cristinne_Correa_Santos.jpg" alt="Alinne Cristinne Corr&ecirc;a dos Santos" />
              <figcaption>
              <h4>Dr&ordf;<br /><span>Alinne Cristinne<br /> Corr&ecirc;a Souza</span></h4>
              <a href="http://lattes.cnpq.br/7003131006996441" target="_blank">
              </a>
              <p class="description"><b>Engenharia de Requisitos</b><br/>P&oacute;s-doutoranda – DCM – USP</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Francisco_Carlos_Monteiro_Souza.jpg" alt="Francisco Carlos Monteiro Souza" />
              <figcaption>
              <h4>Dr&ordm;<br /><span>Francisco Carlos<br /> Monteiro Souza</span></h4>
              <a href="http://lattes.cnpq.br/0057958225738520" target="_blank">
              </a>
              <p class="description"><b>Engenharia de Software baseada em Busca</b><br/>Pós-doutorando – DCM – USP</p>
              </figcaption>
            </figure>
          </div>
        </div>
        <div class="content">
          <div class="grid">
            <figure class="effect-sofiafala">
              <img src="img/content/membros/membro_Lina_Maria_Garces_Rodriguez.jpg" alt="Lina Maria Garc&eacute;s Rodriguez" />
              <figcaption>
              <h4><span>Lina Maria Garc&eacute;s Rodriguez</span></h4>
              <a href="http://lattes.cnpq.br/3525510258366155" target="_blank">
              </a>
              <p class="description"><b>Coordenadora de Desenvolvimento<br />Arquitetura de Software</b><br/>FFCLRP - DCM - USP</p>
              </figcaption>
            </figure>
            <figure class="effect-sofiafala" style="visibility: hidden;">
              <img src="img/content/membros/membro_Lina_Maria_Garces_Rodriguez.jpg" alt="Lina Maria Garc&eacute;s Rodriguez" />
              <figcaption>
              <h4><span>Lina Maria Garc&eacute;s Rodriguez</span></h4>
              <a href="http://lattes.cnpq.br/3525510258366155" target="_blank">
              </a>
              <p class="description"><b>Coordenadora de Desenvolvimento<br />Arquitetura de Software</b><br/>FFCLRP - DCM - USP</p>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    </div>
    
    <!-- RODAPÉ -->
    <?php include_once("resources/template/footer.php"); ?>
  </body>
</html>