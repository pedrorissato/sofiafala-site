<html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <title>SofiaFala - Software Inteligente de Apoio &agrave; Fala</title>
    <?php include_once("resources/template/header.php"); ?>
  </head>

<body>
  <div> 
    <div id="divDownloadUsuario" style="text-align: center">
      O aplicativo SofiaFala é dividido em dois módulos: Criança e Fono.
      <br />
      Se você é paciente, insira o código fornecido por seu(ua) Fonoaudiólogo(a) para fazer o download:

      <br />
      <label for="token">Chave de Acesso:</label>
      <input id="token" type=text></input>
      <button class="btn" onclick="checkDownload(document.getElementById('token').value);"><i class="fa fa-download"></i> Download</button>
    </div>

    <div id="divDownloadFono" style="text-align: center">
      <p>Caso você seja profissional da área de fonoaudiologia, preencha o formulário abaixo, para solicitar acesso e começar a utilizar o SofiaFala com seus pacientes!</p>
    </div>

    <div style="text-align: center">
      <iframe id="formgoogle" src="https://docs.google.com/forms/d/e/1FAIpQLScpeqxCYwrTzZtruLY_oE9AWcxgQdsDsAtafyjloZ-5x2WopA/viewform?embedded=true" width="100%" height="2640" frameborder="0" marginheight="0" marginwidth="0" onload="loaded()">Carregando...</iframe>
    </div>

    <div id="codigosAcesso" style="display: none; text-align: center; border-style: ridge; position: relative; top: -200px;">
      <img id="logoCodigosAcesso" align="left" src="img/layout/logo_sofiafala.jpg" width="20%" height="auto" />
      <h4 style="display: inline-block; font-weight: bold;">Códigos de Acesso<br>
        <h5>Abaixo estão seus códigos de acesso. Cada código permite que 01 (um) paciente faça o download do aplicativo SofiaFala.</h5>
      </h4>
      <table style="text-align: center" id="codesTable" class="table table-bordered">
        <tbody></tbody>
      </table>
    </div>
    
  </div>

  <br />
  <br />
  <br />

  <?php include_once("resources/template/footer.php"); ?>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
</body>

</html>