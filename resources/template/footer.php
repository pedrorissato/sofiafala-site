<!-- <?php if (!$navbar): ?> -->

<footer>

  <hr class="hr-footer">
  <div class="inner-footer">

    <div class="container">

      <div class="row">

        <div class="col-md-4 f-about">

          <a href="index.php"><h1><span>SofiaFala</span></h1></a>

          <p>Software Inteligente de Apoio &agrave; Fala</p>

        </div>

        <div class="col-md-4 l-posts">

          <h3 class="widgetheading">Últimas notícias</h3>

          <ul>

            <!-- <li><a href="http://www.federacaodown.org.br/portal/index.php/noticias/eventos/186-dia-internacional-da-sindrome-de-down-2018/">Dia Internacional da Síndrome de Down 2018</a></li> -->

            <li><a href="http://redeglobo.globo.com/sp/tvvanguarda/vanguardamix/videos/t/edicoes/v/sindrome-de-down/6280049/">

Descobrindo a Sindrome de Down</a></li>

          </ul>

        </div>

        <div class="col-md-4 f-contact">

          <h3 class="widgetheading">Contato</h3>

          <!-- <a href="#"><p><i class="fa fa-envelope"></i> mail@mail.com</p></a> -->

          <p><i class="fa fa-phone"></i>  (16) 3315 4864</p>

          <p><i class="fa fa-home"></i> Av. Bandeirantes, 3900<br />

            Monte Alegre, Ribeirão Preto - SP<br />

            Bloco 1, DCM - FFCLRP - USP</p>

        </div>

      </div>

    </div>

  </div>

<?php endif; ?>

  <div class="last-div">

    <div class="container">

      <div class="row">

        <div class="copyright">

          &copy; SofiaFala - Departamento de Computação e Matemática - USP

          <div class="credits">

          </div>

        </div>

      </div>

    </div>

    <div class="container">

      <div class="row">

        <ul class="social-network">

          <!-- <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook fa-1x"></i></a></li>

          <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter fa-1x"></i></a></li>

          <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus fa-1x"></i></a></li> -->

        </ul>

      </div>

    </div>



    <a href="" class="scrollup"><i class="fa fa-chevron-up"></i></a>



  </div>

</footer>
