<?php
session_start();
//Habilitar para debugar
ini_set('display_errors', 1);

if(!isset($_SESSION['permissao']) || ($_SESSION['permissao'] != 1)){
   echo"<script language='javascript' type='text/javascript'>alert('Você não tem permissão para acessar essa página!');window.location.href='index.php';</script>";
}
$navbar = true;
?>
  <!DOCTYPE html>

  <html xmlns="http://www.w3.org/1999/xhtml">

  <head runat="server">
    <title>Projeto Sofia - An&#225;lise de Segmentos</title>
    <meta charset="utf-8">

    <?php include_once("resources/template/header.php"); ?>
    <?php include_once("resources/template/header-logado.php"); ?>

  </head>

  <body>
    <div class="content">

    <h3 class="text-center">Cadastro de Usuários</h3>
    <br />

    <form class="col-md-offset-1 col-md-11 form-horizontal" enctype="multipart/form-data" action="resources/library/cadastrar-usuario.php" method="POST">


      <input type="hidden" id="id_usuario" readonly="true" value="" />

      <div class="form-group">
        <label for="updFoto" class="col-sm-3 col-md-3 col-form-label">Selecione uma foto:</label>
        <div class="col-sm-4 col-md-3">
          <input id="updFoto" name="Foto" type="file" />
        </div>
      </div>

      <div class="form-group">
        <label for="txtNomeUsuario" class="col-sm-3 col-md-3 col-form-label">Nome do(a) Usuário(a): *</label>
        <div class="col-sm-9 col-md-8">
          <input type="text" id="txtNomeUsuario" name="NomeUsuario" class="form-control" placeholder="Nome Completo" required="required" autofocus="autofocus" />
        </div>
      </div>

      <div class="form-group">
        <label for="txtUsuario" class="col-sm-3 col-md-3 col-form-label">Usuário(a): *</label>
        <span class="glyphicon glyphicon-info-sign glyphicon-large" data-toggle="tooltip" data-original-title="Usuario" title="Usar o primeiro e último nomes da pessoa."></span>
        <div class="col-sm-5 col-md-4">
          <input type="text" id="txtUsuario" name="Usuario" class="form-control" placeholder="Usuario" required="required" autofocus="autofocus" />
        </div>
      </div>

      <div class="form-group">
        <label for="txtEmail" class="col-sm-3 col-md-3 col-form-label">Email(a): *</label>
        <div class="col-sm-8 col-md-7">
          <input type="email" id="txtEmail" name="Email" class="form-control" placeholder="Email" required="required" />
        </div>
      </div>

      <div class="form-group">
        <label for="txtSenha" class="col-sm-3 col-md-3 col-form-label">Senha: *</label>
        <div class="col-sm-6 col-md-5">
          <input type="password" id="txtSenha" name="Senha" class="form-control" placeholder="Senha" required="required" />
        </div>
      </div>

      <div class="form-group">
        <label for="ddlPermissao" class="col-sm-3 col-md-3 col-form-label">Permissão: *</label>
        <div class="col-sm-5 col-md-4">
          <select id="ddlPermissao" class="form-control" name="Permissao">
                    <option value="1">Administrador</option>
                    <option value="2" selected="true">Usuário</option>
                  </select>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-4 col-sm-3 col-md-offset-5 col-md-2">
          <!-- <?php if ($update == false): ?> -->
          <button class="btn btn-primary" type="submit" name="Salvar">Salvar</button>
          <!-- <?php else: ?> -->
          <button class="btn btn-primary" type="submit" name="Atualizar">Atualizar</button>
          <!-- <?php endif ?> -->
        </div>
      </div>
    </form>
</div>

    <?php include_once("resources/template/footer.php"); ?>
  </body>

  </html>
