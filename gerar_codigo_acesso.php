<?php
  include_once("resources/library/funcoes.php");

  $funcoes = new funcoes();

  if($_SERVER['HTTP_REFERER']!=$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"])
  {
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
          $_SERVER['HTTP_X_REQUESTED_WITH']  == 'XMLHttpRequest' )
    {

      $ip = $_POST["ip"];

      $return_response = array();
      $codes = array();

      for ($i=0; $i<=9; $i++){
        array_push($codes, $funcoes->gerar_codigo_acesso());
      }

      try {
        $funcoes->gravar_codigos_gerados($ip, $codes);

        $return_response[] = array("codes" => $codes);

        echo json_encode($return_response);
      }catch (Exception $e){
        echo json_encode($e->getMessage());
      }
    }else{
      header ("Location: index.php");
    }
  }
?>