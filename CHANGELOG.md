# Changelog
As alterações realizadas no site são registradas nesse arquivo.

O formato desse _changelog_ segue o padrão disposto em [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
e o versionamento segue o padrão de [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Em desenvolvimento]: https://git.uspdigital.usp.br/sofiafala/site/compare/v1.0.1...HEAD
- Inserir rotinas que chequem quais movimentos faltam ser realizados.
- Alterar para usar UPDATE no lugar de DELETE.
- Logar todos os logins/tentativas de logins.
- Logar todos DML na tabela resultados.
- Trocar a segurança das senhas de MD5 para SHA-2.

## [1.0.1] - 2018-07-11
### Fixed
- SQJ Injection, inclusive de consultas mais elaboradas como sqlmap.
- A tabela com os resultados foi restaurada.

### Removed
- Registros das escolhas no banco de dados foram zerados (banco foi limpo).
- Banner sobre evento da "Trissomia 21".

## 1.0.0 - 2018-05-13
### Added
- Fotos e dados dos novos integrantes.
- Banner sobre evento da "Trissomia 21".
- Vídeos de beijo, sopro e estalo das fonoaudiólogas.
- Vídeo tutorial na página "Default".
- Design da página de "Default".
- Rotina que verifica vídeos órfãos (2 ou menos) e exclui ao entrar na página.
- Agora ao clicar em "Analisável" ele também faz a rotina de "Próxima Página".

### Fixed
- O número de vídeos já feitos/a fazer.
- Todos os vídeos foram convertidos para .mp4 e alterado o tipo do áudio (era proprietário e por isso o browser não tocava).
- Realinhados os botões de escolha/analisável/lixeira no centro da div.
- Ao clicar em "Analisável" ele preenche o checkbox somente se o campo "Motivo" estiver preenchido.
- Botão "Próxima" estava sendo habilitado também quando digita-se somente ESPAÇOS ou TABS nos motivos. Agora não permite mais.
- Agora na mensagem final aparece o nome do movimento finalizado corretamente.

### Removed
- Registros das escolhas no banco de dados foram zerados.
- Todas as fotos selecionadas previamente foram deletadas.

[Em desenvolvimento]: https://git.uspdigital.usp.br/sofiafala/site/tree/develop
[1.0.1]: https://git.uspdigital.usp.br/sofiafala/site/compare/v1.0.0...v1.0.1
***

_Esse arquivo foi gerado por [pedrorissato](https://git.uspdigital.usp.br/u/10653391), v0.0.2, em 12 de Julho de 2018._
