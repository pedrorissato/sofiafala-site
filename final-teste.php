<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:index.php");
}
$navbar = true;

if (isset($_GET['t'])) {
    if ($_GET['t'] == 1) {
        $tipo = "Beijo";
    } elseif ($_GET['t'] == 2) {
        $tipo = "Estalo de Língua";
    } elseif ($_GET['t'] == 3) {
        $tipo = "Sopro";
    }
} else {
    header("location:/sofiafala/default.php");
}

?>

  <!DOCTYPE html>

  <html xmlns="http://www.w3.org/1999/xhtml">

  <head runat="server">
    <title>Projeto Sofia - An&#225;lise de Segmentos</title>
    <meta charset="utf-8">

    <?php include_once("resources/template/header.php"); ?>
    <?php include_once("resources/template/header-logado.php"); ?>

  </head>

  <body>

    <div class="container">
      <br />
      <br />
      <h1 class="text-center">Parabéns!!!</h1>
      <h1 class="text-center">Você conclui o teste com todos os modelos possíveis de <?= $tipo ?>!</h1>
      <br />
      <h3 class="text-center">O Departamento de Computação e Matemática da USP de Ribeirão Preto em nome de todos os integrantes do Projeto SofiaFala agradece imensamente sua participação!</h3>
      <br />
      <br />
      <br />
    </div>

    <?php include_once("resources/template/footer.php"); ?>
  </body>

  </html>
