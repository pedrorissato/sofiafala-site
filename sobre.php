<html>
  <!DOCTYPE html>

  <html xmlns="http://www.w3.org/1999/xhtml">

  <head runat="server">
    <title>Projeto Sofia - Sobre</title>
    <meta charset="utf-8">

    <?php include_once("resources/template/header.php"); ?>

  </head>

  <body>

<div class="container" id="sobre">
      <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
          <h2>Sobre o SofiaFala</h2>
          <div class="text-justify">
            <p>De acordo com o atual cenário, o projeto tem como principal objetivo apoiar a produção da fala de crianças com síndrome de Down. Atualmente, o projeto possui uma professora, da área de Ciência da Computação, responsável pela coordenação do projeto; uma professora do curso de Fonoaudiologia da FMRP-USP responsável pelo núcleo de fonoaudiologia; 12 pesquisadores-colaboradores, dentre os quais, encontram-se médicos, professores da USP, fonoaudiólogas, psicólogas, terapeutas; e dois bolsistas. As pesquisas a serem conduzidas durante o projeto concentram-se, basicamente, no treinamento para percepção e produção da fala com o auxílio de tecnologias assistivas.
            </p>
          </div>
          <div class="text-center">
              <form method="get" action="img/content/projeto-sofiafala-divulgacao.pdf">
              <button id="btnDivulgacao" name="btnDivulgacao" class="btn btn-lg btn-primary btn-signin" type="submit">Saiba um pouco mais!</button>
            </form>
            </div>
        </div>
      </div>
    </div>

    <!-- <div class="container">
      <br />
      <br />
      <h1 class='text-center'>Bem-vindo</h1>
      <h3 class='text-center'>Para saber mais detalhes sobre o nosso projeto</a></h3>
      <br />
        <div class='row col-xs-12 col-sm-12 col-md-12 justify-content-center align-items-center'>
          <video id='video_meio' class='col-xs-8 col-sm-8 col-md-8' controls>
            <source id='fonteVideo' src="img/content/video_tutorial.mp4" type='video/mp4' />
          </video>
        </div>
      </div>
      </div> -->

    <?php include_once("resources/template/footer.php"); ?>
  </body>
  </html>
